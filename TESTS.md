These are commands that were tested on 3/31 and were determined to work.


## Test of Flavor #1

docker run  -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv --global-var newSpaceKey=MCT --global-var newJiraKey=MCT --global-var "newProjectName=Hello There Friend"

## Test of Flavor #2

docker run  -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Project_Setup_And_Security.json -e ./env/userEnv.json --export-globals globals.json --global-var setupKey=AKS-5385 --global-var secGrouping=ODX

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv -g globals.json

## Test of Flavor #3


docker run  -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Project_Setup_And_Security.json -e ./env/userEnv.json --export-globals globals.json --global-var setupKey=AKS-5385 --global-var secGrouping=ODX

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv -g globals.json


docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/CreateAMCGroupCreation.json -e ./env/userEnv.json  -g globals.json


### Push Living Org Chart To A List of Places:

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
-d ./Project_Automation/config/Dest_List_Example.csv --global-var files="" --global-var parent=Home --global-var "reportName=Living Org Chart" --global-var stripKey="= SPP"  --global-var templatePageId=207781935


### Push Single Report 
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
--global-var files="" --global-var parent=Home --global-var "reportName=Living Org Chart" --global-var stripKey="= SPP"  --global-var templatePageId=207781935 --global-var newJiraKey=MCT --global-var newSpaceKey=MCT


### Test Run Per test Case

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/TestRunPerCase.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=COVID --global-var newSpaceKey=COVID