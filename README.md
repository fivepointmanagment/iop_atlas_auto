# README #

# ***GETTING STARTED***

## AtlasAutomation Pre-Reqs

1. Git installed: (duh) so you can clone this repo and make updates 
2. Docker installed and running
3. Sublime Text: to make edits 
4. Optional: Postman so you can view collections or run them manually

## One time setup (Per Git Clone)
 
- Make a copy of EnvironmentTemplate.json within the env folder
- Rename the copy to **userEnv.json** (caps matter)
- Open new userEnv.json and provide necessary info, which includes the following items:

	- Email address = your ohio.gov email
	- API key 		= your own personal API Token in postman
	- URL 			= odxsupport

## Where should I run this command?
 
##### *Code Type A (MAC OS Users)*
Utilize the **TERMINAL** app to run the commands below. These commands should all be run at the top level of the atlasautomation directory.

##### *Code Type B (WINDOWS OS Users)*
Utilize the **WINDOWS POWERSHELL** app to run the commands below. These commands should all be run at the top level of the atlasautomation directory.

---


# ***PROJECT SETUP***
 
## FLAVOR 1: This command line is for **updating reports** (or creating reports with no additional issues)

##### *FLAVOR 1a (MAC OS)*
```
docker run  -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv --global-var newSpaceKey=XXX --global-var newJiraKey=XXX --global-var "newProjectName=XXX My Project Name Here XXX"
```

##### *FLAVOR 1b (Windows OS)*
```
docker run  -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv --global-var newSpaceKey=XXX --global-var newJiraKey=XXX --global-var "newProjectName=XXX My Project Name Here XXX"
```


## FLAVOR 2: This command line supports the **full process project set up** (creates the Jira Project & Confluence Space, creates the shortcuts betweeen Jira & Confluence, assiagns the security groups, creates a project record, creates reports, and creates APM group creation ticket)

### Input (Variable) Notes For Flavor 2:

* setupKey: You put in the AKS of the project record setup
* secGrouping: You can put in "ODX" or "ODA". It is valid to leave as "XXX" which results in no groups being slotted


### 2- Step 1.) - On Command Line: Run the Following Project Setup Commands All at Once

##### *Step 1a (MAC OS)*
Run the following commands all at once

```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Project_Setup_And_Security.json -e ./env/userEnv.json --export-globals globals.json --global-var setupKey=AKS-XXXX --global-var secGrouping=XXX

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv -g globals.json

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/CreateAPMGroupCreation.json -e ./env/userEnv.json  -g globals.json
```

##### *Step 1b (Windows OS)*
Run the following commands all at once
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Project_Setup_And_Security.json -e ./env/userEnv.json --export-globals globals.json --global-var setupKey=AKS-XXXX --global-var secGrouping=XXX

docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv -g globals.json

docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/CreateAPMGroupCreation.json -e ./env/userEnv.json  -g globals.json
```

### 2- Step 2.) - Update AM ticket and Add Group to Atlas Group field

* Assign the AM ticket to an Ops Team Member
* Add "{Key}-Users" group the the "ATLAS Groups" Custom Field drop down

### 2- Step 3.) - Update Jira Project Settings

* Jira: Update project category to be the same as the ***PILLAR*** that was selected in the project setup request
* Jira: Upload a new image for the project

### 2- Step 4.) - Update Confluence Space Settings

* Confluence: Remove Gliffy Diagram from Confluence Sidebar options
* Confluence: Remove Blog from Confluence Sidebar options by going to Space Settings
* Confluence: Add an image for the project (same as the one from Jira) by going to Space Settings
* Confluence: Update Meetings Repository Action Items to reflect the new project and remove Standard Project Playbook
* Confluence: Update Quieries for Testing Report Pie Charts to reflect the new project and remove SPP

### 2- Step 5.) - Once the AMC Group Creation Ticket is Completed, Complete the following Permissions Setup

* Jira: Add "{Key}-Users" group to Jira Permissions
* Jira: Remove the Project Lead as an individaul user
* Confluence: Remove "Leadership Group" from Confluence Permissions
* Confluence: Remove Yourself and the Project Lead from the INDIVIDUAL USERS section of Confluence Permissions
* Confluence: Add "{Key}-Users" group to Confluence Permissions


## FLAVOR 3: This command line supports the **CORE Project Setup**

### Input (Variable) Notes For Flavor 3:

* setupKey: You put in the AKS # of the project record setup request (**Ensure the setup ticket has been filled out correctly and completely!**)


### 3- Step 1.) - On Command Line: Run the Following Project Setup Commands All at Once. Make sure to update the setupKey variable!
##### Step 1a (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Project_Setup_And_Security-(CORE) -e ./env/userEnv.json --export-globals globals.json --global-var setupKey=AKS-XXXX

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv -g globals.json
```

##### Step 1b (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Project_Setup_And_Security-(CORE) -e ./env/userEnv.json --export-globals globals.json --global-var setupKey=AKS-XXXX

docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/SPP.csv -g globals.json
```

### 3- Step 2.) - Open the new Jira Project and Confluence Space each in a new tab. Then complete the following items:

* In Jira & Confluence: Confirm all of the needed IOP, Pillar, and Key specific groups came through
* In Jira & Confluence: Remove the Project Lead as an individual user
* In Jira & Confluence: Upload an image for the project
* In Confluence: Remove Blog and Gliffy Diagram from Confluence Sidebar options
* In Confluence: Update Meetings Repository Action Items
* In Confluence: Update Quieries for Testing Report Pie Charts
* In Confluence: Upload an image for the project


### 3- Step 3.) - Bulk Upload the Project Role CSV
* In Confluence: Download the ***Project Roles*** CSV from CORE-135 confluence page
* In Jira: Do a bulk upload of the csv file
	* Correlate all of the csv fields to the correct Jira fields
* Ensure all of the tickets came through


### 3- Step 4.) - Bulk Upload the Project Plan CSV
* In Confluence: Download the ***Project Plan*** CSV from CORE-135 confluence page
* In Jira: Do a bulk upload of the csv file
	* Correlate all of the csv fields to the correct Jira fields
	* For ***Issue Type***, select the box to "Map field value". Then make sure that Test Cases map to Test Cases
* Ensure all of the tickets came through, are linked correctly, and are associated to the correct epics


### 3- Step 5.) - On the Command Line: Run the Below Command to Create the Confluence Page for the Templatized Artifacts and Meetings.
##### Step 5a (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Core_Confluence_Template_Pages_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/Core_Artifs_And_Mtgs_Conf_Template_Pages.csv -g globals.json
```

##### Step 5b (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Core_Confluence_Template_Pages_Pusher.json -e ./env/userEnv.json -d ./Project_Automation/config/Core_Artifs_And_Mtgs_Conf_Template_Pages.csv -g globals.json
```


### 3- Step 6.) - On the Command Line: Run the Below Commands to Run the ProjectActivityReport, RequirementsTestCoverage, and TestRunPerTestCase tracing reports. Make sure  you update the newJiraKey and newSpaceKey variables in each command!
##### Step 6a (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/ProjectActivitiesReport.json -e ./env/userEnv.json --global-var "parent=Home" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX --global-var "reportName=Activities Report"

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/ReqToTestCaseTrace.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX

docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/TestRunPerCase.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```


##### Step 6b (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/ProjectActivitiesReport.json -e ./env/userEnv.json --global-var "parent=Home" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX --global-var "reportName=Activities Report"

docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/ReqToTestCaseTrace.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX

docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/TestRunPerCase.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```


### 3- Step 7.) - Add the Template Core Time Tracking Sheet spreadsheet to the New Core Time Tracking Sheet Confluence Page
* In Confluence: Download the template spreadsheet from the CORE-211 confluence page
* In Confluence: Upload the template spreadsheet to the new artifact page
* Ensure all of the meetings and artifacts were created in Jira and in Confluence


---


# ***TRAINING***

## Atlas 101 Training

There is manual setup required. The person records that should have training linked.  The person records needs to be set to "Active" status, assigned to the actual person, and an "Atlas101" label set.  In the future, this can be more automatic

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Training/Atlas101_Auto.json -e ./env/userEnv.json
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Training/Atlas101_Auto.json -e ./env/userEnv.json
```


## Atlas 102 Training

There is manual setup required. The person records that should have training linked.  The person records needs to be set to "Active" status, assigned to the actual person, and an "Atlas102" label set.  In the future, this can be more automatic

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Training/Atlas102_Auto_Ninja.json -e ./env/userEnv.json
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Training/Atlas102_Auto_Ninja.json -e ./env/userEnv.json
```

---



# ***TRACING***

## Project Activities Report

Update the following command with the project keys

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/ProjectActivitiesReport.json -e ./env/userEnv.json --global-var "parent=Home" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX --global-var "reportName=Activities Report"
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/ProjectActivitiesReport.json -e ./env/userEnv.json --global-var "parent=Home" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX --global-var "reportName=Activities Report"
```


## Requirements to Test Case Tracing (Requirements Test Coverage Report)

Update the following command with the project keys

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/ReqToTestCaseTrace.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/ReqToTestCaseTrace.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

## Test Runs Per Test Case 

Update the following command with the project keys

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/TestRunPerCase.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/TestRunPerCase.json -e ./env/userEnv.json --global-var "parent=Testing Reports" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```


## Work Request Tracing Report

Update the following command with the project keys

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Tracing/WorkRequestTracingReport.json -e ./env/userEnv.json --global-var "parent=Home" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX --global-var "reportName=Work Request Tracing Report"
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Tracing/WorkRequestTracingReport.json -e ./env/userEnv.json --global-var "parent=Home" --global-var newJiraKey=XXX --global-var newSpaceKey=XXX --global-var "reportName=Work Request Tracing Report"
```


## Other Tracing Reports

Follow the instructions below and utilize the Test Run to Test Case as a "skeleton template" in Postman. Where you can make modifications within the "Pre-request script" to make any number of meaningful Tracing reports. These are best saved as their own collections.


### Modifying Your Own Tracing Report in Postman

Instructions to Tweak and Run Postman.

1. Recommended setup: Create a new personal workspace in Postman. Import userEnv.json (to your environment) and this collection into a collection.

2. Go to the first call in the collection, "PreCall Variable Init".  Go to the "Pre-request script" section. 

3. You can update the variables to setup the report to go to specific project.  This is generally the first 4 variables. Touch the others at your own risk

4. SAVE the request (and close tab). Otherwise collection will not run correctly.

5. Click the play button the collection (not the individual request). This will open up the collection runner. Choose "Run"

6. Select your imported environment in the dropdown. I also select "Save Responses" and unselect "Keep Variable Values". Run it and be happy.


---


# ***REPORT PUSHING***

In the following sections global variable of parent is used.

### General Notes: parent global var

For reports this comes into play if and only if the  reportName does not already exist. If a reportName already exists and you are pushing a report, the report will be pushed overtop the existing report (as a version) in the current location.

Otherwise, this can do 3 different things:

* --global-var parent=Home will create a new page under the top level report
* --global-var parent=isHome will replace the Overview page
* --global-var "parent=Testing Reports" will follow under "Testing Reports" or whatever page name is specified 

## Push Type 1: Push one Report To Multiple Places/Projects From a Project List

You can push a single report to multiple spaces (and replace keys). The required inputs include:

1. A list of spaces/keys to push
	* This is an .csv file
	* Find example "Dest_List_Example.csv"
2. Globals variables in the command to specify which "template" report
	* *parent* = the parent for the new page (i.e.- if you want this report to show up nested under another page, put the name of that page here
	* *reportName* = the title you want this report to have. The title should match that of the template, just with out the "- Template"
	* *templatePageID* = the page id of the confluence page template in the AM space that you want to push out to a project
	* *newJiraKey* = the project key associated with this particular project space (i.e. - MCT, PMO, etc.)
	* LEAVE STRIP KEY AS SPP. DO **NOT** CHANGE THAT VARIABLE


To create your own list of places to push. Either modify or create a new Dest_List_Example.csv. If you create a new name, update the command below before you run it.

Update the values in the --global-var command below to create new Report pushes:



#### Living Org Chart To MULTIPLE Places:

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
-d ./Project_Automation/config/Dest_List_Example.csv --global-var files="" --global-var parent=Home --global-var "reportName=Living Org Chart" --global-var stripKey="= SPP"  --global-var templatePageId=207781935
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
-d ./Project_Automation/config/Dest_List_Example.csv --global-var files="" --global-var parent=Home --global-var "reportName=Living Org Chart" --global-var stripKey="= SPP"  --global-var templatePageId=207781935
```

#### Artifact Repository To MULTIPLE Places:

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
-d ./Project_Automation/config/Dest_List_Example.csv --global-var files="" --global-var parent=Home --global-var "reportName=Artifact Repository" --global-var stripKey="= SPP"  --global-var templatePageId=195330824
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
-d ./Project_Automation/config/Dest_List_Example.csv --global-var files="" --global-var parent=Home --global-var "reportName=Artifact Repository" --global-var stripKey="= SPP"  --global-var templatePageId=195330824
```


## Push Type 2: Single Report to A SINGLE Specified Location

You can push out single templated reports to a specified destination. The required inputs are:

* *parent* = the parent for the new page (i.e.- if you want this report to show up nested under another page, put the name of that page here
* *reportName* = the title you want this report to have. The title should match that of the template, just with out the "- Template"
* *templatePageID* = the page id of the confluence page template in the AM space that you want to push out to a project
* *newJiraKey* = the project key associated with this particular project space (i.e. - MCT, PMO, etc.)
* LEAVE STRIP KEY AS SPP. DO **NOT** CHANGE THAT VARIABLE


#### Project Overview to One Place:

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
--global-var files="" --global-var parent=isHome --global-var "reportName=Home" --global-var stripKey="= EDUID"  --global-var templatePageId=146769893 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \ --global-var files="" --global-var parent=isHome --global-var "reportName=Home" --global-var stripKey="= EDUID"  --global-var templatePageId=146769893 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

#### Living Org Chart to One Place:

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
--global-var files="" --global-var parent=Home --global-var "reportName=Living Org Chart" --global-var stripKey="= SPP"  --global-var templatePageId=207781935 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \ --global-var files="" --global-var parent=Home --global-var "reportName=Living Org Chart" --global-var stripKey="= SPP"  --global-var templatePageId=207781935 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```


#### Artifact Repository to ONE Place:

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
--global-var files="" --global-var parent=Home --global-var "reportName=Artifact Repository" --global-var stripKey="= SPP"  --global-var templatePageId=195330824 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \ --global-var files="" --global-var parent=Home --global-var "reportName=Artifact Repository" --global-var stripKey="= SPP"  --global-var templatePageId=195330824 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```


#### Meeting Repository to ONE Place:

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
--global-var files="" --global-var parent=Home --global-var "reportName=Meeting Repository" --global-var stripKey="= SPP"  --global-var templatePageId=210568812 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \ --global-var files="" --global-var parent=Home --global-var "reportName=Meeting Repository" --global-var stripKey="= SPP"  --global-var templatePageId=210568812 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```


#### Go-Live Cutover Report:

***NOTE:*** To complete this report it will require manual updating of the 3 JQL's:

1. Cutover Epic JQL *(This should be the key and number for the Cutover Epic in the desired project)* ---> key = {key}-XXX
2. Pre-Cutover JQL *(Key should equal project key and the Epic Link should be the same key/number used for the Cutover Epic)* ---> project = {key} AND "Epic Link" = {key}-XXX and labels in (Pre-Cutover)
3. Cutover JQL *(Key should equal project key and the Epic Link should be the same key/number used for the Cutover Epic)* ---> project = {key} AND "Epic Link" = {key}-XXX and labels in (Cutover)

##### Code Type A (*MAC OS*)
```
docker run -v "$(pwd)":/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \
--global-var files="" --global-var parent=Home --global-var "reportName=Go-Live Cutover Report" --global-var stripKey="= SPP"  --global-var templatePageId=140214400 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```

##### Code Type B (*Windows OS*)
```
docker run -v ${PWD}:/etc/newman -t postman/newman:4.5.4 run ./Project_Automation/Report_Pusher.json -e ./env/userEnv.json \ --global-var files="" --global-var parent=Home --global-var "reportName=Go-Live Cutover Report" --global-var stripKey="= SPP"  --global-var templatePageId=140214400 --global-var newJiraKey=XXX --global-var newSpaceKey=XXX
```